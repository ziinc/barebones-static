/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require("path");
const { createFilePath } = require(`gatsby-source-filesystem`);
const { fmImagesToRelative } = require("gatsby-remark-relative-images");

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({
      node,
      getNode,
      basePath: "content"
    });
    let pageSlug;
    // change the paths
    if (slug == "/home/" || slug == "/home") {
      pageSlug = "/";
    } else {
      pageSlug = slug;
    }
    createNodeField({
      node,
      name: `slug`,
      value: pageSlug
    });
  }
  if (node.internal.type === `ContentJson`) {
    const slug = createFilePath({
      node,
      getNode,
      basePath: "content"
    });
    createNodeField({
      node,
      name: `slug`,
      value: slug
    });
  }

  fmImagesToRelative(node);
};

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions;

  return graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `).then(({ data }) => {
    if (data.allMarkdownRemark == undefined) {
      return;
    }
    data.allMarkdownRemark.edges.forEach(({ node }) => {
      console.log(node);

      const pageSlug = node.fields.slug;
      createPage({
        path: pageSlug,
        component: path.resolve(`src/templates/blogPost.tsx`),
        context: {
          slug: pageSlug
        } // additional data can be passed via context
      });
    });
  });
};
