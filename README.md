# Barebones Static

## Quickstart

```
npm i
npm start
```

## Goals of This Project

To have a stable static website starter that can be easily customized to client requirements.

## Managing Content

All content is managed through a seaprate git repo. This git repo can be managed in any way (normally through netlify CMS, text editors, etc). This encourages a separation of concerns, separating content management and the website design and implementation.

For the actual end user who is using the site, this greatly simplifies things, as the user does not need to look beyond their git content repo to browse their site's content.

For less technical users, Netlify CMS pairs nicely with this workflow, giving a less technical interface for users.

For more technical users, they can manage the content as they please through git.

## Developer Guide

### Content

#### Adding Content Through Git

Git submodules are used to keep content in sync. You can add the git repo with a deployment token, as the content is read-only.

The content folder should be added to the folder named `content` at project root.

Steps:

```bash
# adding to the content folder
git submodule add https://github.com/my/git/repo content

git submodule init

npm start # automatically performs a git sync
```

> **Note** : The git submodule hash needs to be updated by being commited to the repo periodically.

##### Using a Gitlab deployment token with a wiki git repo

This ensures that it will be in read-only mode.

```bash
git submodule add https://my_token_id:my_token_secret@gitlab.com/my_id/my_repo_name.git content
```

### Features

#### Plugins Included

Perform a git clone to obtain this repository's code.

This starter is based on GatsbyJS, and has the following plugins:

- RSS Feeds
- TypeScript
- PostCSS
  - Tailwind CSS
  - AutoPrefixer
    By default, Barebones Static ships with standard web layouts, and common conventions.

#### Components

Main components:

- Navbar
- Footer
- Hero

It also ships with certain helper functions to make life easier with our chosen method of storing data (markdown/json/yml).

### Helper Functions

Several APIs are exposed to accomplish common tasks:

- Parsing a markdown string to html

### Deployment

```bash
npm run build
```

## Contributor Guide

### Tagging a release

Ensure poetry and your python virtual env is set up.

```bash
poetry shell
inv tag-release
```
