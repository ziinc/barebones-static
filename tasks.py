from invoke import task


@task
def tag_release(c, version=None, msg=None):
    current_ver = c.run('git describe', hide=True).stdout
    if version is None:
        version = input('INPUT:::::What version is this release? e.g. 0.1.0\n'
                        'Current version is: {}\n\n'.format(current_ver))
    if msg is None:
        msg = input(
            'INPUT:::::What is this release about? e.g. Some cool message\n')
    print('ACTION:::::Merging develop branch into master.')
    c.run('git checkout master', hide=True)
    c.run('git merge develop', hide=True)
    print('RESULT:::::Merged develop branch into master')

    print('ACTION:::::Tagging project for v{} with message "{}"'.format(version, msg))
    c.run('git tag -a v{} -m "{}"'.format(version, msg), hide=True)
    updated_ver = c.run('git describe', hide=True).stdout
    print('RESULT:::::Updated current version is: {}'.format(updated_ver))

    # checkout to develop again
    print('ACTION:::::Pushing to origin')
    c.run('git checkout master', hide=True)
    c.run('git push origin master', hide=True)
    c.run('git checkout develop', hide=True)
    c.run('git push origin develop', hide=True)
    # push tags
    c.run('git push origin --tags', hide=True)
    print('RESULT:::::Completed push to origin')

    c.run('git checkout develop')
