import React from "react";
import { Navbar } from "../components";
import { MainLayout } from "../layouts";
import { graphql } from "gatsby";
export const template = ({ data }) => {
  // const { html } = data.page;
  const html = "<h1>Hi!</h1>";
  return (
    <MainLayout>
      <div>
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </div>
    </MainLayout>
  );
};
export default template;

// export const pageQuery = graphql`
//   query($path: String!) {
//     page: markdownRemark(fields: { slug: { eq: $path } }) {
//       html
//       excerpt
//       headings {
//         value
//         depth
//       }
//       frontmatter {
//         title
//       }
//     }
//   }
// `;
