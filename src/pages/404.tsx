import React from "react";
import { MainLayout } from "../layouts";
import { graphql } from "gatsby";

const NotFoundPage = () => {
  return (
    <MainLayout className="blog-post-container">
      <div className="blog-post">
        <div>
          <h1>NOT FOUND</h1>
          <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
        </div>
      </div>
    </MainLayout>
  );
};

export default NotFoundPage;

// export const pageQuery = graphql`
//   query {
//     settings: markdownRemark(fields: { slug: { eq: "/" } }) {
//       frontmatter {
//         siteName
//       }
//     }
//   }
// `;
