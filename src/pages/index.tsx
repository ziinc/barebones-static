import * as React from "react";
import { MainLayout } from "../layouts";
export const template = ({ data }) => {
  // const { html } = data.page;
  const html = "<h1>Hi!</h1>";
  return (
    <MainLayout>
      <div>
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </div>
    </MainLayout>
  );
};
export default template;
