import * as React from "react";

import { Link } from "gatsby";
interface NavLink {
  to: string;
  text: string;
}
export const Navbar = ({ navLinks }: { navLinks: NavLink[] }) => {
  return (
    <div className="bg-grey w-full">
      {navLinks.map(({ to, text }) => (
        <Link to={to}>{text}</Link>
      ))}
    </div>
  );
};
