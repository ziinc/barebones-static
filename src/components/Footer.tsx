import * as React from "react";

import { Link } from "gatsby";

interface FooterLink {
  to: string;
  text: string;
}
interface FooterSection {
  header: string;
  links: FooterLink[];
}
interface Logo {
  src: string; // img src
  to: string; // on click
}
interface Props {
  copyrightText?: string;
  logo?: Logo;
  footerSections?: FooterSection[];
}

export const Footer = ({ copyrightText, logo, footerSections }: Props) => {
  return (
    <div className="bg-grey w-full">
      {copyrightText ? `©  ${copyrightText}` : null}
    </div>
  );
};
