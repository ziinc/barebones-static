import React from "react";
import { MainLayout } from "../layouts";
import { Navbar } from "../components";
import { graphql } from "gatsby";

const template = ({ data }) => {
  // const { html, frontmatter } = data.page;
  const html = "<h1>Hello!</h1>";
  return (
    <MainLayout>
      <div dangerouslySetInnerHTML={{ __html: html }} />
    </MainLayout>
  );
};
export default template;

// export const pageQuery = graphql`
//   query($path: String!) {
//     page: markdownRemark(fields: { slug: { eq: $path } }) {
//       html
//       excerpt
//       headings {
//         value
//         depth
//       }
//       frontmatter {
//         title
//       }
//     }
//   }
// `;
