import * as React from "react";
import { Navbar, Footer } from "../components";
import { graphql, useStaticQuery, Link } from "gatsby";
import Helmet from "react-helmet";
export const MainLayout = ({ children }) => {
  // const { settings } = useStaticQuery(query);
  // const { siteName, navbar, footer } = settings;
  const { siteName, navbar, footer } = {};
  const renderHead = () => {
    if (!siteName) {
      return null;
    }

    if (pageTitle) {
      return (
        <title>
          {pageTitle} - {siteName}
        </title>
      );
    } else {
      return <title>{siteName}</title>;
    }
  };

  return (
    <div className="flex flex-col h-screen flex-grow justify-between">
      <Helmet>{renderHead()}</Helmet>
      <Navbar navLinks={[{ to: "/", text: "Home" }]} />
      <div className="flex-grows">{children}</div>
      <Footer copyrightText="My Project" />
    </div>
  );
};

// const query = graphql`
//   query {
//     settings: contentJson {
//       siteName
//       navbar {
//         navLinks {
//           text
//           to
//         }
//       }
//       footer {
//         copyrightText
//       }
//     }
//   }
// `;
