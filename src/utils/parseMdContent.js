import remark from "remark";
import html from "remark-html";
import grayMatter from "gray-matter";

/**
 * This function parses a string of markdown with front matter.
 * @returns {Object} with the following characteristics:
 *  - contents key with raw html content. Html content is not sanitized.
 *  - markdown key with raw markdown string with front matter.
 *  - metadata key for all  front matter.
 * @example
 *  >>> const result = parseMdContent('---title: Some Front Matter---\n\n # This is a title\n Some content')
 *  >>> console.log(result)
 *  {
 *      html: "<h1>This is a title</h1><p>Some content</p>",
 *      metadata: {
 *          title: "Some Front Matter"
 *      }
 *  }
 */
export const parseMdContent = async mdString => {
  let metadata;
  let mdStringWithoutFrontMatter;
  if (grayMatter.test(mdString) === true) {
    // there is yaml front matter, only want the data key
    //rename to metadata variable
    const grayMatterResults = grayMatter(mdString);
    metadata = grayMatterResults.data;
    mdStringWithoutFrontMatter = grayMatterResults.content;
  } else {
    mdStringWithoutFrontMatter = mdString;
  }
  // add html, rename to htmlString variable
  const { contents: htmlString } = await remark()
    .data({
      gfm: true,
      commonmark: true,
      footnotes: true,
      pendantic: true
    })
    .use(html)
    .process(mdStringWithoutFrontMatter);

  return {
    metadata: metadata ? metadata : {},
    html: htmlString
  };
};
