import { parseMdContent } from "./parseMdContent";
test("should return a json object with correct schema", async function() {
  const testInput = "# Some markdown\nTest content hello";
  const result = await parseMdContent(testInput);
  expect(Object.keys(result)).toContain("html");
  expect(Object.keys(result)).toContain("metadata");

  expect(result.html).toContain("<h1>");
  expect(result.html).toContain("Some markdown");
  expect(result.html).toContain("Test content hello");
});
test("when no frontmatter provided, will return empty object as metadata", async function() {
  const testInput = "# Some markdown\nTest content hello";
  const result = await parseMdContent(testInput);
  expect(result.metadata).toEqual({});
});

test("should parse yaml front matter correctly", async function() {
  const titleVal = "SomeTitle";
  const testInput = `---\ntitle: "${titleVal}"\n---\n# Some markdown\nTest content hello`;
  const result = await parseMdContent(testInput);
  expect(result.html).toBeDefined;
  expect(result.html).not.toContain(titleVal);
  expect(Object.keys(result.metadata)).toContain("title");
  expect(result.metadata.title).toBe(titleVal);
});
